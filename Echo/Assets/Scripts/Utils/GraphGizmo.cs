using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WordsOnPlay.Utils;


namespace WordsOnPlay.Utils {

public class GraphGizmo : MonoBehaviour
{
    [SerializeField]
    private float duration = 30; // seconds
    [SerializeField]
    private float range = 1;
    [SerializeField]
    private Rect rect;
    [SerializeField]
    private Color rectColor = Color.green;
    [SerializeField]
    private Color line1Color = Color.yellow;
    [SerializeField]
    private Color line2Color = Color.red;

    private Queue<float> values1 = new Queue<float>();
    private Queue<float> values2 = new Queue<float>();
    private Queue<float> times = new Queue<float>();

    public void Clear() 
    {
        values1.Clear();
        values2.Clear();
        times.Clear();
    }

    public void Add(float v1, float v2)
    {
        values1.Enqueue(v1);
        values2.Enqueue(v2);
        times.Enqueue(Time.time);
    }

    public void Update()
    {
        while (times.Count > 0 && times.Peek() < Time.time - duration)
        {
            values1.Dequeue();
            values2.Dequeue();
            times.Dequeue();
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = rectColor;
        rect.DrawGizmo();

        IEnumerator<float> v1It = values1.GetEnumerator();
        IEnumerator<float> v2It = values2.GetEnumerator();
        IEnumerator<float> tIt = times.GetEnumerator();

        bool first = true;
        Vector3 p01 = Vector3.zero;
        Vector3 p02 = Vector3.zero;

        while (v1It.MoveNext() && v2It.MoveNext() && tIt.MoveNext())
        {
            float x = 1 - (Time.time - tIt.Current) / duration;
            float y1 = v1It.Current / range;
            float y2 = v2It.Current / range;

            Vector3 p11 = rect.Point(x,y1);
            Vector3 p12 = rect.Point(x,y2);

            if (first)
            {
                first = false;
            }
            else 
            {
                Gizmos.color = line1Color;
                Gizmos.DrawLine(p01, p11);
                Gizmos.color = line2Color;
                Gizmos.DrawLine(p02, p12);
            }
            p01 = p11;
            p02 = p12;
        }
    }

}
}