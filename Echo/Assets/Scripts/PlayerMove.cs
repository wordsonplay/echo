using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMove : MonoBehaviour
{
    [SerializeField]
    private float speed = 2;

    private new Rigidbody2D rigidbody;
    private Vector2 velocity;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();    
    }

    void FixedUpdate()
    {
        rigidbody.velocity = velocity;
    }

    void Update()
    {
        velocity.x = Input.GetAxis("Horizontal");
        velocity.y = Input.GetAxis("Vertical");

        velocity = velocity.normalized * Mathf.Max(Mathf.Abs(velocity.x), Mathf.Abs(velocity.y));
        velocity *= speed;
    }
}
