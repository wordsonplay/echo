using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField]
    private int roomWidth = 19;
    [SerializeField]
    private int roomHeight = 9;
    [SerializeField]
    private AnimationCurve curve;
    [SerializeField]
    private float moveDuration = 0.2f;

    private Transform player;
    private int x = 0;
    private int y = 0;

    private float cameraDistance;
    private Vector3 moveFrom;
    private Vector3 moveTo; 
    private float moveTimer = 0;

    void Start()
    {
        cameraDistance = transform.position.z;
        transform.position = CalculatePos();
        player = GameObject.FindObjectOfType<PlayerMove>().transform;
    }

    void Update()
    {
        if (moveTimer > 0) {
            Move();
        }
        else {
            CheckPlayerPosition();
        }
    }

    private Vector3 CalculatePos()
    {
        return new Vector3(x * roomWidth, y * roomHeight, cameraDistance);
    }

    private void CheckPlayerPosition()
    {
        Vector2 pos = 2 * (player.position - transform.position);

        int dx = 0;
        int dy = 0;

        if (pos.x < -roomWidth)
        {
            dx = -1;
        }
        else if (pos.x > roomWidth)
        {
            dx = 1;
        }

        if (pos.y < -roomHeight)
        {
            dy = -1;
        }
        else if (pos.y > roomHeight)
        {
            dy = 1;
        }

        if (dx != 0 || dy != 0)
        {
            moveTimer = moveDuration;
            x += dx;
            y += dy;
            moveFrom = transform.position;
            moveTo = CalculatePos();
        }
    }

    private void Move() 
    {
        moveTimer = Mathf.Clamp01(moveTimer - Time.deltaTime);
        float t = 1 - moveTimer / moveDuration;
        t = curve.Evaluate(t);
        transform.position = Vector3.Lerp(moveFrom, moveTo, t);
    }
}
