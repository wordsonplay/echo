using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WordsOnPlay.Utils;

[RequireComponent(typeof(Wavefront))]
[RequireComponent(typeof(AudioSource))]
public class Spectrum : MonoBehaviour
{
    [SerializeField]
    private Rect rect;
    [SerializeField]
    private Color borderColor = Color.green;
    [SerializeField]
    private Color lineColor = Color.yellow;
    [SerializeField]
    private Color avColor = Color.red;
    [SerializeField]
    private Color binColor = Color.grey;
    [SerializeField]
    private float scale = 10;
    [SerializeField]
    public float weight = 0.999f;
    [SerializeField]
    public int nBins = 1024;
    [SerializeField]
    public int graphBin = 1;
    [SerializeField]
    public int amp = 1;

    private AudioSource audio;
    private GraphGizmo graph;
    private Wavefront wavefront;

    private float[] spectrum;
    private float[] prevSpectrum;
    private float[] avSpectrum;

    void Start()    
    {
        audio = GetComponent<AudioSource>();
        graph = GetComponent<GraphGizmo>(); 
        wavefront = GetComponent<Wavefront>();
        spectrum = new float[nBins];
        prevSpectrum = new float[nBins];
        avSpectrum = new float[nBins];
    }

    void Update()
    {
        float old = avSpectrum[graphBin];
        audio.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);

        float w = Mathf.Pow(weight, Time.deltaTime);
        for (int i = 0; i < spectrum.Length; i++) 
        {   
            avSpectrum[i] = Mathf.Max(w * avSpectrum[i], spectrum[i] - prevSpectrum[i], 0);
        }

        if (avSpectrum[graphBin] > old)
        {
            float strength = Mathf.Clamp01(amp * (avSpectrum[graphBin] - old));
            wavefront.AddPulse(strength);
        }

        graph?.Add(spectrum[graphBin] - prevSpectrum[graphBin], avSpectrum[graphBin]);

        for (int i = 0; i < spectrum.Length; i++) 
        {   
            prevSpectrum[i] = spectrum[i];
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = borderColor;
        Gizmos.DrawLine(rect.Point(0,0), rect.Point(1,0));

        float x = (float)graphBin / nBins;
        Vector3 p0 = rect.Point(x, 0);
        Vector3 p1 = rect.Point(x, 1);
        Gizmos.color = binColor;
        Gizmos.DrawLine(p0, p1);

        if (spectrum != null)
        {
            for (int i = 0; i < spectrum.Length; i++)
            {
                x = (float)i / spectrum.Length;
                float y = scale * spectrum[i];

                p0 = rect.Point(x, 0);
                p1 = rect.Point(x, y);
                Gizmos.color = lineColor;
                Gizmos.DrawLine(p0, p1);

                float dx = 0.5f / spectrum.Length;
                y = scale * avSpectrum[i];
                p0 = rect.Point(x-dx, y);
                p1 = rect.Point(x+dx, y);
                Gizmos.color = avColor;
                Gizmos.DrawLine(p0, p1);
            }
        }
    }
}
