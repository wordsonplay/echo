using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wavefront : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particlesPrefab;    

    public void AddPulse(float strength)
    {
        ParticleSystem particles = Instantiate(particlesPrefab);
        particles.transform.parent = transform;
        particles.transform.localPosition = Vector3.zero;
        particles.transform.localRotation = Quaternion.identity;
        Destroy(particles.gameObject, particles.main.duration);

        ParticleSystem.Burst burst = particles.emission.GetBurst(0);
        short count = (short)(Mathf.RoundToInt(strength * particles.main.maxParticles));
        burst.minCount = count;
        burst.maxCount = count; 
        particles.emission.SetBurst(0,burst);
    }
}